package santadon.p3b.gamep3b;


public interface InterfaceFragmentStart {

    void changeToFragmentGame();

    void changeToFragmentEnd();

    void setScoreName(String name);
}
