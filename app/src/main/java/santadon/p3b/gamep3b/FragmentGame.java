package santadon.p3b.gamep3b;


import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Bundle;
import android.app.Fragment;
import android.os.CountDownTimer;
import android.view.*;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Locale;
import java.util.Random;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentGame extends Fragment implements View.OnClickListener, View.OnTouchListener {

    private TextView tvScoreCounter;
    private TextView tvTime;
    private TextView tvCountdown;

    private ImageView ivGame;
    private ImageView ivTargetObject;

    private Bitmap mBitmapGame;
    private Bitmap mBitmapTargetObject;
    private Canvas mGameCanvas;
    private Canvas mTargetObjectCanvas;

    private Button btnFinish;

    private GestureDetector detector;

    private InterfaceFragmentGame activity;

    private int scoreCounter;

    private InterfaceShape shapes[];

    private CountDownTimer gameCountdown;
    private CountDownTimer gameTime;

    private int target;

    private Setting setting;

    private int canvasColor = Color.BLUE;


    public FragmentGame() {
        // Required empty public constructor
    }

    public static FragmentGame newInstance(InterfaceFragmentGame activity) {
        FragmentGame frag = new FragmentGame();
        frag.activity = activity;
        return frag;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View result = inflater.inflate(R.layout.fragment_game, container, false);

        this.tvScoreCounter = result.findViewById(R.id.tv_score_counter);
        this.tvTime = result.findViewById(R.id.tv_waktu);

        this.ivGame = result.findViewById(R.id.iv_game);
        this.ivTargetObject = result.findViewById(R.id.iv_target_object);

        this.btnFinish = result.findViewById(R.id.btn_finish);
        this.detector = new GestureDetector(new SimpleGestureListener());
        this.tvCountdown = result.findViewById(R.id.tv_countdown);

        this.scoreCounter = 0;

        this.setting = new Setting(this.getActivity());

//      DISINI BUAT NGATUR BERAPA BANYAK OBJEK YANG KELUAR
        this.shapes = new InterfaceShape[setting.loadTargetAmount()];

        this.btnFinish.setOnClickListener(this);

//      PARAMETER MILLISINFUTURE YANG INI NENTUIN COUNTDOWN MULAI GAME
        this.gameCountdown = new CountDownTimer(3000, 1000) {
            @Override
            public void onTick(long l) {
                tvCountdown.setText(String.format("%d", (l / 1000) + 1));
            }

            @Override
            public void onFinish() {
                initializeBitmap();
                drawShapes();
                ivGame.setOnTouchListener(FragmentGame.this);
//              PARAMETER MILLISINFUTURE YANG INI NENTUIN DURASI GAME
                FragmentGame.this.gameTime = new CountDownTimer(setting.loadTime() * 1000, 1000) {
                    @Override
                    public void onTick(long l) {
                        tvTime.setText(String.format("Time: %d", (l / 1000) + 1));
                    }


                    @Override
                    public void onFinish() {
                        finish();
                    }
                }.start();

                ((ViewGroup) tvCountdown.getParent()).removeView(tvCountdown);
            }
        }.start();

        return result;
    }

    private void initializeBitmap() {
        this.mBitmapGame = Bitmap.createBitmap(this.ivGame.getWidth(), this.ivGame.getHeight(), Bitmap.Config.ARGB_4444);
        this.mBitmapTargetObject = Bitmap.createBitmap(this.ivTargetObject.getWidth(), this.ivTargetObject.getHeight(), Bitmap.Config.ARGB_4444);

        this.ivGame.setImageBitmap(this.mBitmapGame);
        this.ivTargetObject.setImageBitmap(this.mBitmapTargetObject);

        this.mGameCanvas = new Canvas(mBitmapGame);
        this.mTargetObjectCanvas = new Canvas(mBitmapTargetObject);

        this.mGameCanvas.drawColor(this.canvasColor);
        this.mTargetObjectCanvas.drawColor(this.canvasColor);
    }

    public static String[] generateRandomWords(int numberOfWords) {
        String[] randomStrings = new String[numberOfWords];
        Random random = new Random();
        for (int i = 0; i < numberOfWords; i++) {
            // DISINI YANG NENTUIN BERAPA HURUF YANG MUNCUL
            char[] word = new char[6];
            for (int j = 0; j < word.length; j++) {
                word[j] = (char) ('a' + random.nextInt(26));
            }
            randomStrings[i] = new String(word);
        }
        return randomStrings;
    }

    private InterfaceShape getNonIntersectShape(String shape, String word, int color) {
        Random rand = new Random();

        int left = rand.nextInt(this.ivGame.getWidth() - 250);
        int top = rand.nextInt(this.ivGame.getHeight() - 250);

        InterfaceShape result = null;

        if (shape.equalsIgnoreCase("square")) {
            result = new Square(left, top, word, color);
        } else if (shape.equalsIgnoreCase("circle")) {
            result = new Circle(left, top, word, color);
        }

        Rect rect = new Rect(left, top, left + 250, top + 250);

        for (int i = 0; i < this.shapes.length && this.shapes[i] != null; i++) {
            while (rect.intersect(this.shapes[i].getCoordinate()[0], this.shapes[i].getCoordinate()[1], this.shapes[i].getCoordinate()[2], this.shapes[i].getCoordinate()[3])) {
                left = rand.nextInt(this.ivGame.getWidth() - 250);
                top = rand.nextInt(this.ivGame.getHeight() - 250);

                if (shape.equalsIgnoreCase("square")) {
                    result = new Square(left, top, word, color);
                } else if (shape.equalsIgnoreCase("circle")) {
                    result = new Circle(left, top, word, color);
                }

                rect = new Rect(left, top, left + 250, top + 250);

                i = 0;
            }
        }

        return result;
    }

    private void generateNonInstersectShapes(){
        Random rand = new Random();

        for (int i = 0; i < shapes.length; i++) {
            String word = generateRandomWords(1)[0];
            int color = Color.argb(255, rand.nextInt(256), rand.nextInt(256), rand.nextInt(256));

            int shapeNumber = rand.nextInt(2);
            String shape;

            switch (shapeNumber) {
                case 0:
                    shape = "square";
                    break;
                case 1:
                    shape = "circle";
                    break;
                default:
                    shape = "hexagon";
                    break;
            }

            this.shapes[i] = null;

            int left = rand.nextInt(this.ivGame.getWidth() - 250);
            int top = rand.nextInt(this.ivGame.getHeight() - 250);

            if (shape.equalsIgnoreCase("square")) {
                this.shapes[i] = new Square(left, top, word, color);
            } else if (shape.equalsIgnoreCase("circle")) {
                this.shapes[i] = new Circle(left, top, word, color);
            }

            Rect rect = new Rect(left, top, left + 250, top + 250);

            int breaker = 0;

            for (int j = 0; j < i; j++) {
                while (rect.intersect(this.shapes[j].getCoordinate()[0], this.shapes[j].getCoordinate()[1], this.shapes[j].getCoordinate()[2], this.shapes[j].getCoordinate()[3])) {
                    left = rand.nextInt(this.ivGame.getWidth() - 250);
                    top = rand.nextInt(this.ivGame.getHeight() - 250);

                    if (shape.equalsIgnoreCase("square")) {
                        this.shapes[i] = new Square(left, top, word, color);
                    } else if (shape.equalsIgnoreCase("circle")) {
                        this.shapes[i] = new Circle(left, top, word, color);
                    }

                    rect = new Rect(left, top, left + 250, top + 250);

                    j = 0;
                }

                breaker++;
                if(breaker > 1000){
                    i = 0;
                    break;
                }
            }
        }
    }

    private void drawShapes() {
        Random rand = new Random();

        this.target = rand.nextInt(this.shapes.length);

        this.generateNonInstersectShapes();

        for (int i = 0; i < shapes.length; i++) {
            this.shapes[i].drawIt(this.mGameCanvas);
        }

        if (this.setting.loadColor() != 0) {
            int color;

            switch (this.setting.loadColor()) {
                case 1:
                    color = Color.RED;
                    break;
                case 2:
                    color = Color.WHITE;
                    break;
                case 3:
                    color = Color.CYAN;
                    break;
                case 4:
                    color = Color.DKGRAY;
                    break;
                case 5:
                    color = Color.MAGENTA;
                    break;
                case 6:
                    color = Color.YELLOW;
                    break;
                case 7:
                    color = Color.GREEN;
                    break;
                default:
                    color = Color.BLACK;
            }

            this.shapes[this.target].setColor(color);
            this.shapes[this.target].drawIt(mGameCanvas);
        }
        this.shapes[this.target].drawItInTargetCanvas(this.mTargetObjectCanvas);

        this.ivGame.invalidate();
        this.ivTargetObject.invalidate();
    }

    private void finish() {
        this.activity.addScoresToFragmentEnd(scoreCounter);
        this.gameCountdown.cancel();
        if (this.gameTime != null) {
            this.gameTime.cancel();
        }
        this.activity.changeToFragmentEnd();
    }

    @Override
    public void onClick(View view) {
        if (view == this.btnFinish) {
            this.finish();
        }
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        return this.detector.onTouchEvent(motionEvent);
    }

    private class SimpleGestureListener extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onDown(MotionEvent e) {
            for (int i = 0; i < shapes.length; i++) {
                if (shapes[i].checkClicked(e.getX(), e.getY())) {
                    if (i == target) {
                        scoreCounter++;
                    } else {
                        scoreCounter--;
                    }

                    tvScoreCounter.setText(String.format(Locale.ENGLISH, "Score: %d", scoreCounter));

                    mGameCanvas.drawColor(canvasColor);
                    mTargetObjectCanvas.drawColor(canvasColor);

                    drawShapes();
                    break;
                }
            }

            return true;
        }
    }
}
