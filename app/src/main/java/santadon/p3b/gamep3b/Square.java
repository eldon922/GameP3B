package santadon.p3b.gamep3b;

import android.graphics.*;

public class Square implements InterfaceShape {
    private int left, top, right, bottom, size;
    private String word;
    private int color;

    public Square(int left, int top, String word, int color) {
        this.left = left;
        this.top = top;

        this.size = 250;

        this.right = left + size;
        this.bottom = top + size;

        this.word = word;

        this.color = color;
    }

    public int[] getCoordinate(){
        return new int[]{left, top, right, bottom};
    }

    @Override
    public void drawIt(Canvas mCanvas) {
        Rect rect = new Rect(this.left, this.top, this.right, this.bottom);

        Paint paintRect = new Paint();
        paintRect.setColor(color);

        Paint paintText = new Paint();
        paintText.setColor(Color.BLACK);
        paintText.setTextSize(50);
        paintText.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));

        mCanvas.drawRect(rect, paintRect);
        mCanvas.drawText(this.word, rect.centerX() - 100, rect.centerY(), paintText);
    }

    @Override
    public void drawItInTargetCanvas(Canvas mCanvas) {
        Rect rect = new Rect(mCanvas.getWidth() / 2 - size/2, mCanvas.getHeight() / 2 - size/2, mCanvas.getWidth() / 2 + size/2, mCanvas.getHeight() / 2 + size/2);

        Paint paint = new Paint();
        paint.setColor(color);

        Paint paintText = new Paint();
        paintText.setColor(Color.BLACK);
        paintText.setTextSize(50);
        paintText.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));

        mCanvas.drawRect(rect, paint);
        mCanvas.drawText(this.word, rect.centerX() - 100, rect.centerY(), paintText);
    }

    @Override
    public boolean checkClicked(float x, float y) {
        return x >= left && x <= right && y >= top && y <= bottom;
    }

    @Override
    public void setColor(int color) {
        this.color = color;
    }
}
