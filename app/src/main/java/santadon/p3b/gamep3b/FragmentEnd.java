package santadon.p3b.gamep3b;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentEnd extends Fragment implements View.OnClickListener {

    protected ListView lvScore;
    protected Button btnMainMenu;
    protected InterfaceFragmentEnd activity;
    protected ScoreAdapter scoreAdapter;

    public FragmentEnd() {
        // Required empty public constructor
    }

    public static FragmentEnd newInstance(InterfaceFragmentEnd activity) {
        FragmentEnd frag = new FragmentEnd();
        frag.activity = activity;
        frag.scoreAdapter = new ScoreAdapter(activity.getLayoutInflater());
        return frag;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View res = inflater.inflate(R.layout.fragment_end, container, false);

        this.lvScore = res.findViewById(R.id.lv_score);
        this.btnMainMenu = res.findViewById(R.id.btn_main_menu);

        this.lvScore.setAdapter(scoreAdapter);

        this.btnMainMenu.setOnClickListener(this);

        this.scrollLvScoreToBottom();

        return res;
    }

    public void addScore(Score score) {
        this.scoreAdapter.add(score);
    }

    public int getCountScores() {
        return this.scoreAdapter.getCount();
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == this.btnMainMenu.getId()) {
            activity.changeToFragmentStart();
        }
    }

    private void scrollLvScoreToBottom() {
        this.lvScore.post(new Runnable() {
            @Override
            public void run() {
                lvScore.setSelection(lvScore.getCount() - 1);
            }
        });
    }
}
