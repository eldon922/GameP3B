package santadon.p3b.gamep3b;

import android.view.LayoutInflater;

public interface InterfaceFragmentEnd {

    void changeToFragmentStart();

    LayoutInflater getLayoutInflater();
}
