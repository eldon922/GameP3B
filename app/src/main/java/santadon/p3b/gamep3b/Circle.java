package santadon.p3b.gamep3b;

import android.graphics.*;

public class Circle implements InterfaceShape {
    private int left, top, right, bottom, radius;
    private String word;
    private int color;

    public Circle(int left, int top, String word, int color) {
        this.left = left;
        this.top = top;
        
        this.radius = 125;
        
        this.right = left + radius*2;
        this.bottom = top + radius*2;

        this.word = word;

        this.color = color;
    }

    public int[] getCoordinate(){
        return new int[]{left, top, right, bottom};
    }

    @Override
    public void drawIt(Canvas mCanvas) {
        Rect rect = new Rect(this.left, this.top, this.right, this.bottom);

        Paint paintCircle = new Paint();
        paintCircle.setColor(color);

        Paint paintText = new Paint();
        paintText.setColor(Color.BLACK);
        paintText.setTextSize(50);
        paintText.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));

        mCanvas.drawCircle(this.left + radius, this.top + radius, radius, paintCircle);
        mCanvas.drawText(this.word, rect.centerX() - 100, rect.centerY(), paintText);
    }

    @Override
    public void drawItInTargetCanvas(Canvas mCanvas) {
        Rect rect = new Rect(mCanvas.getWidth() / 2 - radius, mCanvas.getHeight() / 2 - radius, mCanvas.getWidth() / 2 + radius, mCanvas.getHeight() / 2 + radius);

        Paint paintCircle = new Paint();
        paintCircle.setColor(color);

        Paint paintText = new Paint();
        paintText.setColor(Color.BLACK);
        paintText.setTextSize(50);
        paintText.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));

        mCanvas.drawCircle(mCanvas.getWidth() / 2, mCanvas.getHeight() / 2, radius, paintCircle);
        mCanvas.drawText(this.word, rect.centerX() - 100, rect.centerY(), paintText);
    }

    @Override
    public boolean checkClicked(float x, float y) {
        return x >= left && x <= right && y >= top && y <= bottom;
    }

    @Override
    public void setColor(int color) {
        this.color = color;
    }
}
