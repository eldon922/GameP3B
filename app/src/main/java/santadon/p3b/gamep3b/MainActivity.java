package santadon.p3b.gamep3b;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity implements InterfaceFragmentStart, InterfaceFragmentGame, InterfaceFragmentEnd {

    private FragmentManager fragmentManager;
    private FragmentStart fragmentStart;
    private FragmentGame fragmentGame;
    private FragmentEnd fragmentEnd;

    private Score score;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.fragmentManager = this.getFragmentManager();

        this.fragmentStart = FragmentStart.newInstance(this);
        this.fragmentGame = FragmentGame.newInstance(this);
        this.fragmentEnd = FragmentEnd.newInstance(this);

        this.changeToFragmentStart();
    }

    @Override
    public void changeToFragmentStart() {
        this.score = new Score();

        FragmentTransaction transaction = this.fragmentManager.beginTransaction();
        transaction.replace(R.id.frag_container, this.fragmentStart);
        transaction.commit();
    }

    @Override
    public void changeToFragmentGame() {
        FragmentTransaction transaction = this.fragmentManager.beginTransaction();
        transaction.replace(R.id.frag_container, this.fragmentGame);
        transaction.commit();
    }

    @Override
    public void changeToFragmentEnd() {
        FragmentTransaction transaction = this.fragmentManager.beginTransaction();
        transaction.replace(R.id.frag_container, this.fragmentEnd);
        transaction.commit();
    }

    @Override
    public void setScoreName(String name) {
        if (name.equals("")) {
            score.setName(String.format("Player #%d", this.fragmentEnd.getCountScores() + 1));
        } else {
            score.setName(name);
        }
    }

    @Override
    public void addScoresToFragmentEnd(int scoreValue) {
        score.setScore(scoreValue);
        this.fragmentEnd.addScore(this.score);
    }
}
