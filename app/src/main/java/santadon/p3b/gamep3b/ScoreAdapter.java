package santadon.p3b.gamep3b;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

public class ScoreAdapter extends BaseAdapter {
    private List<Score> scores;
    private LayoutInflater inflater;
    private ScoreDatabaseHandler scoreStorage;

    public ScoreAdapter(LayoutInflater inflater) {
        this.scoreStorage = new ScoreDatabaseHandler(inflater.getContext());
        this.scores = scoreStorage.getAllScores();
        this.inflater = inflater;
    }

    public void add(Score score) {
        this.scores.add(score);
        this.scoreStorage.addScore(score);
        this.notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return scores.size();
    }

    @Override
    public Object getItem(int i) {
        return  this.scores.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View itemView = this.inflater.inflate(R.layout.list_view_score, null);

        TextView tvNumber = itemView.findViewById(R.id.tv_number);
        TextView tvName = itemView.findViewById(R.id.tv_name);
        TextView tvScore = itemView.findViewById(R.id.tv_score);

        tvNumber.setText(Integer.toString(i + 1));
        tvName.setText(scores.get(i).getName());
        tvScore.setText(Integer.toString(scores.get(i).getScore()));

        if (i == this.getCount() - 1) {
            itemView.setBackgroundResource(android.R.color.darker_gray);
        }

        return itemView;
    }
}
