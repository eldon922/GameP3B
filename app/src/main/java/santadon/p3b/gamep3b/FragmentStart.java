package santadon.p3b.gamep3b;

import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.Locale;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentStart extends Fragment implements View.OnClickListener {

    protected Button btnStart;
    protected Button btnHistory;

    protected EditText etPlayerName;

    protected InterfaceFragmentStart activity;

    protected Setting setting;

    protected EditText etTargetAmount;
    protected EditText etTime;

    protected Spinner sColor;

    public FragmentStart() {
        // Required empty public constructor
    }

    public static FragmentStart newInstance(InterfaceFragmentStart activity) {
        FragmentStart frag = new FragmentStart();
        frag.activity = activity;
        return frag;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View res = inflater.inflate(R.layout.fragment_start, container, false);
        this.btnStart = res.findViewById(R.id.btn_start);
        this.btnHistory = res.findViewById(R.id.btn_history);

        this.etPlayerName = res.findViewById(R.id.et_player_name);

        this.btnStart.setOnClickListener(this);
        this.btnHistory.setOnClickListener(this);

        this.setting = new Setting(this.getActivity());

        this.etTargetAmount = res.findViewById(R.id.et_target_amount);

        this.etTime = res.findViewById(R.id.et_time);

        this.sColor = res.findViewById(R.id.s_color);

        return res;
    }

    @Override
    public void onResume() {
        super.onResume();
        this.etTargetAmount.setText(String.format(Locale.ENGLISH, "%d", this.setting.loadTargetAmount()));
        this.etTime.setText(String.format(Locale.ENGLISH, "%d", this.setting.loadTime()));
        this.sColor.setSelection(this.setting.loadColor());
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == this.btnStart.getId()) {
            this.activity.setScoreName(this.etPlayerName.getText().toString());

            this.activity.changeToFragmentGame();

            this.etPlayerName.setText("");

            if (!this.etTargetAmount.getText().toString().equals("")&&!this.etTargetAmount.getText().toString().equals("0")) {
                this.setting.saveTargetAmount(Integer.parseInt(this.etTargetAmount.getText().toString()));
            }

            if (!this.etTime.getText().toString().equals("")) {
                this.setting.saveTime(Integer.parseInt(this.etTime.getText().toString()));
            }

            this.setting.saveColor(this.sColor.getSelectedItemPosition());
        } else if (view.getId() == this.btnHistory.getId()) {
            this.activity.changeToFragmentEnd();
        }
    }
}
