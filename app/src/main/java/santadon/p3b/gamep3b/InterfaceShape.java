package santadon.p3b.gamep3b;

import android.graphics.Canvas;

public interface InterfaceShape {

    int[] getCoordinate();

    void drawIt(Canvas mCanvas);

    void drawItInTargetCanvas(Canvas mCanvas);

    boolean checkClicked(float x, float y);

    void setColor(int color);
}
