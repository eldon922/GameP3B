package santadon.p3b.gamep3b;


public interface InterfaceFragmentGame {

    void changeToFragmentEnd();

    void addScoresToFragmentEnd(int scoreValue);
}
