package santadon.p3b.gamep3b;

import android.content.Context;
import android.content.SharedPreferences;

public class Setting {
    private SharedPreferences sharedPref;
    private final static String NAMA_SHARED_PREF = "sp_setting";
    private final static String KEY_TARGET_AMOUNT = "TARGET_AMOUNT";
    private static final String KEY_TIME = "TIME";
    private final static String KEY_COLOR = "COLOR";

    public Setting(Context context) {
        this.sharedPref = context.getSharedPreferences(NAMA_SHARED_PREF, 0);
    }

    public void saveTargetAmount(int targetAmount) {
        SharedPreferences.Editor editor = this.sharedPref.edit();
        editor.putInt(KEY_TARGET_AMOUNT, targetAmount);
        editor.commit();
    }

    public void saveTime(int time) {
        SharedPreferences.Editor editor = this.sharedPref.edit();
        editor.putInt(KEY_TIME, time);
        editor.commit();
    }

    public void saveColor(int color) {
        SharedPreferences.Editor editor = this.sharedPref.edit();
        editor.putInt(KEY_COLOR, color);
        editor.commit();
    }

    public int loadTargetAmount() {
        return sharedPref.getInt(KEY_TARGET_AMOUNT, 5);
    }

    public int loadTime() {
        return sharedPref.getInt(KEY_TIME, 30);
    }

    public int loadColor() {
        return sharedPref.getInt(KEY_COLOR, 0);
    }
}
